import threading
import tkinter
import tkinter.messagebox as msg
import os
import sys
from selenium import webdriver


def open_chrome():
    global driver
    path = os.path.dirname(sys.argv[0])
    driver = webdriver.Chrome(path + '/chromedriver')
    driver.get('https://www.google.com')


def alert():
    msg.showinfo('サイトリロード', 'これまでと違った内容が表示されました')


def retry():
    global driver, source
    driver.refresh()
    if source != driver.page_source:
        alert()
    else:
        print("リロード")
        t = threading.Thread(target=retry)
        t.setDaemon(True)
        t.start()


def start_check():
    global driver, source
    source = driver.page_source
    t = threading.Thread(target=retry)
    t.setDaemon(True)
    t.start()


root = tkinter.Tk()
root.title(u"自動リロードツール")

Button = tkinter.Button(text=u'ブラウザ起動', command=open_chrome,)
Button.pack()
Button2 = tkinter.Button(text=u'エラー画面記録', command=start_check,)
Button2.pack()

root.mainloop()